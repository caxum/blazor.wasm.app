#pragma checksum "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "227c74cc80b80d39fa0e8e0ff33c8e14d1b226c3"
// <auto-generated/>
#pragma warning disable 1591
namespace Blazor.WASM.APP
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Blazor.WASM.APP;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\GrowthAndLearning\Blazor.WASM.APP\_Imports.razor"
using Blazor.WASM.APP.Shared;

#line default
#line hidden
#nullable disable
    public partial class _Imports : System.Object
    {
        #pragma warning disable 1998
        protected void Execute()
        {
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
